
#instal required packages
install.packages("tidyverse")
install.packages("purrr")
install.packages("factoextra")
install.packages("VIM")
install.packages("date") 
install.packages("ggplot2")
install.packages("gplots")
install.packages("ggmap")
install.packages("aod")
install.packages("lubridate")
install.packages("scales")
install.packages("gridExtra")
install.packages("ggthemes")
install.packages("UsingR")

# load required librarires
library(factoextra)   #To create a beautiful graph of the clusters generated 
#with the kmeans() function, will use the factoextra package.
library(date)
library(lubridate) # for working with dates
library(ggplot2)  # for creating graphs
library(scales)   # to access breaks/formatting functions
library(gridExtra) # for arranging plots
library(dplyr)
library(ggmap)
library(gridExtra)
library(tidyverse)
library(purrr)

#select data set file
data <- read.csv(file.choose(),header=T)

set.seed(101) # Set Seed so that same sample can be reproduced in future also
# Now Selecting 75% of data as sample from total 'n' rows of the data  
sample <- sample.int(n = nrow(data), size = floor(.02*nrow(data)), replace = F)
train <- data[sample, ]
test  <- data[-sample, ]


#6.1 Scatter plot
plot(sample, main = "SCATTER PLOT",
     pch = 19, frame = FALSE)

plot(data$connectedWifi_Level)
plot(data$procs_running)
plot(data$Total_CPU)
plot(data$Traffic_timestamp)

#6.2
df <- scale(sample) # Scaling the data
# information regarding data set.
str(sample)
summary(sample)
head(sample)
# VIM library for using 'aggr'
library(VIM)
# 'aggr' plots the amount of missing/imputed values in each column
aggr(sample)


#6.1 Scatter plot
plot(sample, main = "SCATTER PLOT",
     pch = 19, frame = FALSE)



top.n.custs <- function (sample,cols,n=5) { #Requires some data frame and the top N to remove
  idx.to.remove <-integer(0) #Initialize a vector to hold customers being removed
  for (c in cols){ # For every column in the data we passed to this function
    col.order <-order(sample[,c],decreasing=T) #Sort column "c" in descending order (bigger on top)
    #Order returns the sorted index (e.g. row 15, 3, 7, 1, ...) rather than the actual values sorted.
    idx <-head(col.order, n) #Take the first n of the sorted column C to
    idx.to.remove <-union(idx.to.remove,idx) #Combine and de-duplicate the row ids that need to be removed
  }
  return(idx.to.remove) #Return the indexes of customers to be removed
}
top.custs <-top.n.custs(data,cols=3:8,n=5)
length(top.custs) #How Many Customers to be Removed?
data[top.custs,] #Examine the customers
data.rm.top<-data[-c(top.custs),] #Remove the Customers

set.seed(76964057) #Set the seed for reproducibility
k <-kmeans(dat.rm.top[,-c(1,10)], centers=5) #Create 5 clusters, Remove columns 1 and 2
k$centers #Display&nbsp;cluster centers
table(k$cluster) #Give a count of data points in each cluster







# Compute k-means with k = 2
set.seed(123)
km.res <- kmeans(df, 2, nstart = 25)
#showing the fist5 rows of data
head(df, n = 3)

print(km.res)

#It's possible to compute the mean of each variables by clusters using the original data
aggregate(sample, by=list(cluster=km.res$cluster), mean)

#I want to add the point classifications to the original data
pc <- cbind(sample, cluster = km.res$cluster)
head(pc)

plot(pc)
##################################
### LET US ACCESS THE RESULTS ###
##################################

# Cluster number for each of the observations
km.res$cluster

head(km.res$cluster, 4)
#[1] 1 1 1 1

# Cluster size
km.res$size
#[1] 509 517

# Cluster means
km.res$centers
#    X.0.584498519700527   X.0.0358725055864216
#1          -0.7699387           -0.5068869
#2           0.7580248            0.4990434

#clusterr plot using fviz_cluster
library(cluster)
library("factoextra")
fviz_cluster(km.res, data = df)

#We can execute the same process for 3, 4, and 5 clusters
k3 <- kmeans(df, centers = 3, nstart = 25)
k4 <- kmeans(df, centers = 4, nstart = 25)
k5 <- kmeans(df, centers = 5, nstart = 25)

# plots to compare
p1 <- fviz_cluster(km.res, geom = "point", data = df) + ggtitle("k = 2")
p2 <- fviz_cluster(k3, geom = "point",  data = df) + ggtitle("k = 3")
p3 <- fviz_cluster(k4, geom = "point",  data = df) + ggtitle("k = 4")
p4 <- fviz_cluster(k5, geom = "point",  data = df) + ggtitle("k = 5")

library(gridExtra)
grid.arrange(p1, p2, p3, p4, nrow = 2)

#### 6 (D)

avg_sil <- function(k) {
  km.res <- kmeans(df, centers = k, nstart = 25)
  ss <- silhouette(km.res$cluster, dist(df))
  mean(ss[, 3])
}
# Compute and plot wss for k = 2 to k = 20
k.values <- 2:15
library(purrr)
# extract avg silhouette for 2-15 clusters
avg_sil_values <- map_dbl(k.values, avg_sil)

plot(k.values, avg_sil_values,
     type = "b", pch = 19, frame = FALSE, 
     xlab = "K",
     ylab = "Total number of clusters")


##########################################################
############ 6.2 Spectral Clustering #####################

install.packages('igraph')
install.packages('cluster')
install.packages("anocva")

library(anocva)

spectral_clustering <- function(sample, # matrix of data points
                                nn = 10, # the k nearest neighbors to consider
                                n_eig = 2) # m number of eignenvectors to keep
{
  mutual_knn_graph <- function(sample, nn = 10)
  {
    D <- as.matrix( dist(sample) ) # matrix of euclidean distances between data points in zz
    
    # intialize the knn matrix
    knn_mat <- matrix(0,
                      nrow = nrow(sample),
                      ncol = nrow(sample))
    
    # find the 10 nearest neighbors for each point
    for (i in 1: nrow(sample)) {
      neighbor_index <- order(D[i,])[2:(nn + 1)]
      knn_mat[i,][neighbor_index] <- 1 
    }
    
    # Now we note that i,j are neighbors iff K[i,j] = 1 or K[j,i] = 1 
    knn_mat <- knn_mat + t(knn_mat) # find mutual knn
    
    knn_mat[ knn_mat == 2 ] = 1
    
    return(knn_mat)
  }
  
  graph_laplacian <- function(W, normalized = TRUE)
  {
    stopifnot(nrow(W) == ncol(W)) 
    
    g = colSums(W) # degrees of vertices
    n = nrow(W)
    
    if(normalized)
    {
      D_half = diag(1 / sqrt(g) )
      return( diag(n) - D_half %*% W %*% D_half )
    }
    else
    {
      return( diag(g) - W )
    }
  }
  
  W = mutual_knn_graph(sample) # 1. matrix of similarities
  L = graph_laplacian(W) # 2. compute graph laplacian
  ei = eigen(L, symmetric = TRUE) # 3. Compute the eigenvectors and values of L
  n = nrow(L)
  return(ei$vectors[,(n - n_eig):(n - 1)]) # return the eigenvectors of the n_eig smallest eigenvalues
  
}

# do spectral clustering procedure

X_sc <- spectral_clustering(sample)

# run kmeans on the 2 eigen vectors
X_sc_kmeans <- kmeans(X_sc, 3)

plot( df,X_sc,
      # type = "b", pch = 19, frame = FALSE, 
      xlab = "K",
      ylab = "spectral clustering")


sample$connectedWifi_Level <- ifelse(data$connectedWifi_Level == "-127", 1, 0)

for(i in 1:21) {
  sample[, i] <- as.numeric(as.character(df[, i]))
}

library(caret)
'%ni%' <- Negate('%in%')  # define 'not in' func
options(scipen=999)  # prevents printing scientific notations.
set.seed(100)

trainDataIndex <- createDataPartition(sample$connectedWifi_Level, p=0.7, list = F)
trainData <- data[trainDataIndex, ]
testData <- data[-trainDataIndex, ]
table(train$connectedWifi_Level)

set.seed(100)
down_train <- downSample(x = train[, colnames(train) %ni% "Class"],y = train$connectedWifi_Level)
table(down_train$connectedWifi_Level)

# set.seed(100)
# up_train <- upSample(x = trainData[, colnames(trainData) %ni% "Class"],y = trainData$procs_running)
# table(up_train$procs_running)

logitmod <- glm(procs_running ~ CPU_0 + CPU_1 + CPU_2 + CPU_3 + Total_CPU + TotalMemory_freeSize + TotalMemory_max_size + TotalMemory_total_size + TotalMemory_used_size + Traffic_MobileRxBytes + Traffic_MobileRxPackets + Traffic_MobileTxBytes + Traffic_MobileTxPackets + Traffic_TotalRxBytes + Traffic_TotalRxPackets + Traffic_TotalTxBytes + Traffic_TotalTxPackets + Traffic_TotalWifiRxBytes + Traffic_TotalWifiRxPackets + connectedWifi_Level, family = "binomial", data=down_train)

glm.D93 <- glm(procs_running ~  Traffic_MobileRxPackets + Traffic_MobileTxBytes + Traffic_MobileTxPackets + Traffic_TotalRxBytes + Traffic_TotalRxPackets + Traffic_TotalTxBytes + Traffic_TotalTxPackets + Traffic_TotalWifiRxBytes + Traffic_TotalWifiRxPackets + connectedWifi_Level, family = poisson())
anova(glm.D93)
summary(logitmod)

pred <- predict(logitmod, newdata = test, type = "response")
pred

y_pred_num <- ifelse(pred > 0.5, 1, 0)
y_pred <- factor(y_pred_num, levels=c(0, 1))
y_act <- testData$malicious

mean(y_pred == y_act)

confint.default(logitmod)
exp(coef(logitmod))
with(logitmod, null.deviance - deviance)



library(cluster)
km.res <- kmeans(data, 4, nstart = 25)
# Visualize
library("factoextra")
fviz_cluster(km.res, data = df, frame.type = "convex")+
  theme_minimal()

plot(cluster)







